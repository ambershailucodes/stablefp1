//
//  AirControllerViewController.swift
//  inputs
//
//  Created by sailesh Subramaniam (Contractor) on 6/17/19.
//  Copyright © 2019 com.fp.pageinputs. All rights reserved.
//

import UIKit

class AirControllerViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onLand(_ sender: Any) {
        performSegue(withIdentifier: "gotomainstoryboard", sender: self)
    }
}
